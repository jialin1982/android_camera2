package com.geniteam.camera2cv;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.TextureView;
import android.widget.Toast;

public class CameraActivity extends Activity {
    private static final String TAG = "CameraActivity";
    static final int REQUEST_CAMERA = 1;

    private AutoFitTextureView mCameraTextureView = null;
    private Camera2Manager mCameraManager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mCameraTextureView = (AutoFitTextureView) findViewById(R.id.camera_texture_view);
        mCameraManager = new Camera2Manager(this, mCameraTextureView);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the App
                Toast.makeText(this, "You cannot use thi app without granting permission", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                mCameraTextureView = (AutoFitTextureView) findViewById(R.id.camera_texture_view);
                mCameraManager = new Camera2Manager(this, mCameraTextureView);
                Log.d(TAG, "Preview set");
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCameraManager != null)
            mCameraManager.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCameraManager != null)
            mCameraManager.onPause();
    }
}
